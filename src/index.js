import React, { Component } from "react";
import ReactDOM from "react-dom";
import MasteryTable from "./components/mastery-table";

class App extends Component {
  constructor() {
    super();
    this.state = {
      pointsAllocated: 0
    };
  }

  callback = points => {
    if (!this.checkCapped(points)) {
      this.setState({ pointsAllocated: this.state.pointsAllocated + points });
    }
  };

  checkCapped = points => {
    if (this.state.pointsAllocated + points > 1145) {
      return true;
    } else {
      return false;
    }
  };

  render() {
    return (
      <div>
        <span>Points Required: {this.state.pointsAllocated}</span>
        <MasteryTable
          Class="Holy Knight"
          callback={this.callback}
          checkCapped={this.checkCapped}
        />
      </div>
    );
  }
}

ReactDOM.render(<App />, window.document.getElementById("root"));

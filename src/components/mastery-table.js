import React, { Component } from "react";
import MasteryRow from "./mastery-row";
import classMasteries from "../masteries.json";

class MasteryTable extends Component {
  render() {
    const { Class } = this.props;
    const masteries = classMasteries.find(c => c.Class === Class);
    const ulStyle = {
      listStyle: "none"
    };
    return (
      <ul style={ulStyle}>
        <li>
          <MasteryRow
            row="1"
            masteries={masteries.Master}
            callback={this.props.callback}
            checkCapped={this.props.checkCapped}
          />
        </li>
        <li>
          <MasteryRow
            row="2"
            masteries={masteries.Advanced}
            callback={this.props.callback}
            checkCapped={this.props.checkCapped}
          />
        </li>
        <li>
          <MasteryRow
            row="3"
            masteries={masteries.Expert}
            callback={this.props.callback}
            checkCapped={this.props.checkCapped}
          />
        </li>
        <li>
          <MasteryRow
            row="4"
            masteries={masteries.Ultimate}
            callback={this.props.callback}
            checkCapped={this.props.checkCapped}
          />
        </li>
      </ul>
    );
  }
}

export default MasteryTable;

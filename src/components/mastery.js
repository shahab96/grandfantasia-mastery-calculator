import React, { Component } from "react";

class Mastery extends Component {
  constructor(props) {
    super(props);
    this.incrementLevel = this.incrementLevel.bind(this);
    this.decrementLevel = this.decrementLevel.bind(this);
    this.state = {
      level: 0,
      pointsAllocated: 0
    };
  }

  incrementLevel() {
    let { level } = this.state;
    let { pointsAllocated } = this.state;
    let newPoints = 0;

    if (level < 20) {
      level++;
      newPoints = this.props.row * 2;
    } else return;

    if (level > 10 && level <= 15) {
      newPoints += 3;
    } else if (level > 15) {
      newPoints += 7;
    }

    pointsAllocated += newPoints;

    if (this.props.checkCapped(newPoints)) {
      return;
    }
    this.setState({
      level,
      pointsAllocated
    });

    this.props.callback(newPoints);
  }

  decrementLevel() {
    let { level } = this.state;
    let { pointsAllocated } = this.state;
    let newPoints = 0;

    if (level > 0) {
      level--;
      newPoints = this.props.row * 2;
    } else return;

    if (level >= 10 && level < 15) {
      newPoints += 3;
    } else if (level >= 15) {
      newPoints += 7;
    }

    pointsAllocated -= newPoints;
    newPoints *= -1;

    this.setState({
      level,
      pointsAllocated
    });

    this.props.callback(newPoints);
  }

  render() {
    const divStyle = {
      margin: "0, 2px"
    };
    const spanStyle = {
      margin: "0 auto"
    };
    const imgStyle = {
      height: "20px",
      width: "20px"
    };
    return (
      <div style={divStyle}>
        <img style={imgStyle} src={this.props.imgsrc} alt="img" />
        <button onClick={this.decrementLevel}>-</button>
        <button onClick={this.incrementLevel}>+</button>
        <br />
        <span style={spanStyle}>{this.state.level}</span>
      </div>
    );
  }
}

export default Mastery;

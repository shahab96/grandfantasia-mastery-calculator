import React, { Component } from "react";
import Mastery from "./mastery";

class MasteryRow extends Component {
  render() {
    const ulStyle = {
      listStyleType: "none",
      margin: 0,
      padding: 0,
      overflow: "hidden",
      backgroundColor: "#333333"
    };
    const liStyle = {
      float: "left"
    };
    return (
      <ul style={ulStyle}>
        {this.props.masteries.map(mastery => (
          <li style={liStyle}>
            <Mastery
              mastery={mastery}
              row={this.props.row}
              callback={this.props.callback}
              checkCapped={this.props.checkCapped}
            />
          </li>
        ))}
      </ul>
    );
  }
}

export default MasteryRow;
